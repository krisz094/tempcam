let ws;
const diagBtn = document.getElementById('measure-button');
//const resetBtn = document.getElementById('restart-button');
const camIMG = document.getElementById('cam-img2');
const temperatureElem = document.getElementById('temperature')

const hideImage = () => {
    if (camIMG) {
        camIMG.classList.add('hidden');
    }
}

const showImage = () => {
    if (camIMG) {
        camIMG.classList.remove('hidden');
    }
}

const hideAllStates = () => {
    document.querySelectorAll('.diagnosis-state').forEach(elem => elem.classList.add('hidden'))
}

const sendResetSignal = () => {
    ws.send(JSON.stringify({
        code: "restartCamera",
        payload: true
    }));
}

const confirmBeforeResetSignal = () => {
    const result = confirm("Biztosan újra szeretné indítani a kamera szervert?");
    if (result) {
        sendResetSignal();
    }
}

function setupWS() {
    hideAllStates();
    hideImage();
    const dcElem = document.getElementById('is-disconnected');
    ws = new WebSocket(wsHost + "/backend");

    ws.onmessage = (event) => {
        const message = JSON.parse(event.data);
        if (message.code === "newState") {
            hideAllStates();
            const elem = document.querySelector(`[data-stateid="${message.payload}"]`);
            elem.classList.remove('hidden');
            if (message.payload == 0) {
                diagBtn.removeAttribute("disabled");
            }
            else {
                diagBtn.setAttribute("disabled", "disabled");
            }
            if (message.payload == -1) {
                hideImage();
            }
            else {
                showImage();
            }
        }

        else if (message.code === "newStateExtra") {
            const elem = document.querySelector(`[data-stateid="${message.payload}"] .state-extra`);
            if (elem) {
                elem.innerHTML = message.payloadExtra;
            }
        }
        else if (message.code === "newImage") {
            camIMG.src = message.payload;
        }
        else if (message.code === "newTemps") {
            temperatureElem.innerHTML = Math.round(message.payload * 100) / 100;
        }
    }
    ws.onopen = () => {
        dcElem.classList.add('hidden');
        showImage();
    }
    ws.onerror = ws.onclose = () => {
        if (dcElem) {
            dcElem.classList.remove('hidden');
        }
        hideAllStates();
        hideImage();
        setTimeout(() => setupWS(), 5000);
    }
}

diagBtn.onclick = () => {
    ws.send(JSON.stringify({
        code: "startDiagnosis",
        payload: true
    }));
}

//resetBtn.onclick = confirmBeforeResetSignal;

hideAllStates();
setupWS();