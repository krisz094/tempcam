let ws;
const fullscreenDiv = document.getElementById('fullscreen-div')
const camIMG = document.getElementById('cam-img');
const fullScreenBtn = document.getElementById('fullscreen-button')

const hideImage = () => {
    if (camIMG) {
        camIMG.classList.add('hidden');
    }
}

const showImage = () => {
    if (camIMG) {
        camIMG.classList.remove('hidden');
    }
}

const hideAllStates = () => {
    document.querySelectorAll('.diagnosis-state').forEach(elem => elem.classList.add('hidden'))
}

fullScreenBtn.onclick = () => {
    fullscreenDiv.requestFullscreen();
}

function setupWS() {
    hideAllStates();
    hideImage();
    ws = new WebSocket(wsHost + "/frontend")

    ws.onmessage = (event) => {
        const message = JSON.parse(event.data);
        if (message.code === "newImage") {
            camIMG.src = message.payload;
        }
        else if (message.code === "newState") {
            hideAllStates();
            const elem = document.querySelector(`[data-stateid="${message.payload}"]`);
            elem.classList.remove('hidden');
            if (message.payload == -1) {
                hideImage();
            }
            else {
                showImage();
            }
        }
        else if (message.code === "newStateExtra") {
            const elem = document.querySelector(`[data-stateid="${message.payload}"] .state-extra`);
            if (elem) {
                elem.innerHTML = message.payloadExtra;
            }
        }
    }
    ws.onopen = () => {
        showImage();
    }
    ws.onerror = ws.onclose = () => {
        hideAllStates();
        hideImage();
        setTimeout(() => setupWS(), 5000);
    }
}

hideAllStates();
setupWS();