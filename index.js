const express = require('express');
const { spawn } = require('child_process');
const path = require('path');

const PY27PATH = path.join('c:\\\\', 'python27', 'python.exe');

const PYSCRIPTPATH = path.join(__dirname, 'pythonCode', 'ThermalViewer.py');
let pythonProcess = spawn(PY27PATH, [PYSCRIPTPATH]);
const app = express();
const expressWs = require('express-ws')(app);

const STATES = {
    CAM_OFF: -1,
    READY: 0,
    DIAGNOSIS_RUNNING: 1,
    ENTRY_OK: 2,
    ENTRY_FORBIDDEN: 3,
    CAM_ERR: 4
}

class Entry {
    /**
     * 
     * @param {number} date 
     * @param {number} temperature 
     */
    constructor(date, temperature) {
        this.date = date;
        this.temperature = temperature;
    }
}

const randNumBtwn = (min, max) => {
    const diff = max - min;
    return min + Math.random() * diff;
}

/**
 * @type {Entry[]}
 */
const entries = [
];

/**
 * @type {WebSocket[]}
 */
const connectedFrontendWebSockets = [];

/**
 * @type {WebSocket[]}
 */
const connectedBackendWebSockets = [];

let state = STATES.READY;


/**
 * 
 * @param {number} date 
 * @param {number} temperature 
 */
const addEntry = (date, temperature) => {
    const entry = new Entry(date, temperature);
    console.log('added entry', new Date(date), temperature)
    entries.push(entry);
    connectedBackendWebSockets.forEach(ws => {
        if (ws.readyState === ws.OPEN) {
            ws.send(JSON.stringify({
                code: "newEntry",
                payload: entry
            }));
        }
    });
}

function sendToEveryWS(obj) {
    [connectedFrontendWebSockets, connectedBackendWebSockets]
        .forEach(wsArray => wsArray.forEach(ws => {
            if (ws.readyState === ws.OPEN) {
                ws.send(JSON.stringify(obj));
            }
        }));
}

const changeStateTo = newState => {
    state = newState;
    sendToEveryWS({
        code: "newState",
        payload: state
    });
}

const waitFor = time => {
    return new Promise(res => {
        setTimeout(() => {
            return res();
        }, time);
    });
}

const NEEDED_DIAGNOSIS_SAMPLES = 10;
/**
 * @type {number[]}
 */
let diagnosisTemps = [];

const startDiagnosis = async () => {
    if (state === STATES.READY) {
        diagnosisTemps = [];
        changeStateTo(STATES.DIAGNOSIS_RUNNING)
    }
}

const gotEnoughDiagnosisTemps = async () => {
    const date = +new Date();

    const temperature = getArrayAvg(diagnosisTemps);
    diagnosisTemps = [];
    addEntry(date, temperature);

    if (temperature > 37) {
        changeStateTo(STATES.ENTRY_FORBIDDEN);
        sendToEveryWS({
            code: "newStateExtra",
            payload: STATES.ENTRY_FORBIDDEN,
            payloadExtra: Math.round(temperature * 100) / 100
        })
    }
    else {
        changeStateTo(STATES.ENTRY_OK);
        sendToEveryWS({
            code: "newStateExtra",
            payload: STATES.ENTRY_OK,
            payloadExtra: Math.round(temperature * 100) / 100
        })
    }

    await waitFor(3500);

    changeStateTo(STATES.READY);
}

/**
 * 
 * @param {WebSocket} ws 
 */
const backendWSHandler = ws => {
    connectedBackendWebSockets.push(ws);

    ws.onmessage = event => {
        const msg = JSON.parse(event.data);
        if (msg.code === "startDiagnosis" && msg.payload === true) {
            startDiagnosis();
        }
    }

    ws.send(JSON.stringify({
        code: "newState",
        payload: state
    }));

    ws.send(JSON.stringify({
        code: "entries",
        payload: entries
    }));

    ws.onclose = () => {
        const idx = connectedBackendWebSockets.findIndex(x => x === ws);
        connectedBackendWebSockets.splice(idx, 1);
    }
}

app.ws('/backend', backendWSHandler);

/**
 * 
 * @param {WebSocket} ws 
 */
const frontendWSHandler = ws => {
    connectedFrontendWebSockets.push(ws);
    ws.send(JSON.stringify({
        code: "newState",
        payload: state
    }));

    ws.onclose = () => {
        const idx = connectedFrontendWebSockets.findIndex(x => x === ws);
        connectedFrontendWebSockets.splice(idx, 1);
    }
}
app.ws('/frontend', frontendWSHandler);

/**
 * @type {number[]}
 */
let currentTemps = [];

const getArrayAvg = arr => arr.reduce((p, c) => +p + +c, 0) / arr.length;

/**
 * 
 * @param {WebSocket} ws 
 */
const pythonWSHandler = ws => {
    ws.onmessage = event => {
        const msg = JSON.parse(event.data);
        if (msg.code === "b64img") {
            sendToEveryWS({
                code: "newImage",
                payload: 'data:image/jpg;base64,' + msg.payload
            });
        }
        else if (msg.code === "temps") {
            currentTemps = msg.payload;
            const avg = getArrayAvg(currentTemps);
            if (state === STATES.DIAGNOSIS_RUNNING) {
                diagnosisTemps.push(avg);
                sendToEveryWS({
                    code: "newStateExtra",
                    payload: STATES.DIAGNOSIS_RUNNING,
                    payloadExtra: Math.round(diagnosisTemps.length / NEEDED_DIAGNOSIS_SAMPLES * 100)
                })
                if (diagnosisTemps.length >= NEEDED_DIAGNOSIS_SAMPLES) {
                    gotEnoughDiagnosisTemps();
                }
            }

            sendToEveryWS({
                code: "newTemps",
                payload: avg
            });
        }
        if (state === STATES.CAM_OFF) {
            changeStateTo(STATES.READY);
        }
    }

    ws.onerror = ws.onclose = () => {
        changeStateTo(STATES.CAM_OFF);
    }
}

app.ws('/python', pythonWSHandler);

app.use('/', express.static('static'));

changeStateTo(STATES.CAM_OFF);

app.listen(3000, () => { console.log('listening on port http://127.0.0.1:3000') });